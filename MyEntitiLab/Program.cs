﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyEntitiLab
{
    class Program
    {
        public class CustomerInfo
        {
            public int Row { get; set; }
        }

        public static void showData()
        {
            using (var db = new EF_Model())
            {
                var ds = (from c in db.CUSTOMER select c).ToList();
                foreach (var item in ds)
                {
                    Console.WriteLine(string.Format("{0}|{1}|{2}"
                 , item.CUSTOMER_ID
                 , item.NAME
                 , item.EMAIL
                 , item.COUNTRY_CODE
                 , item.BUDGET
                 , item.USED));
                }
            }

        }
        public static void selectData()
        {
            using (var db = new EF_Model())
            {
                //1.select ข้อมูล[CUSTOMER_ID] ,[NAME] ,[EMAIL] ,[COUNTRY_NAME]
                //,[BUDGET] ,[USED] จาก Table[dbo].[CUSTOMER]
                //และ[dbo].[COUNTRY] โดยมีเงื่อนไข[COUNTRY_CODE] = 'US'

                var ds = (from c in db.CUSTOMER
                          join cn in db.COUNTRY on c.COUNTRY_CODE equals cn.COUNTRY_CODE
                          where c.COUNTRY_CODE == "US"
                          select new
                          {
                              c.CUSTOMER_ID,
                              c.NAME,
                              c.EMAIL,
                              cn.COUNTRY_NAME,
                              c.BUDGET,
                              c.USED
                          }).ToList();

                foreach (var item in ds)
                {
                    Console.WriteLine(string.Format("{0}|{1}|{2}"
                 , item.CUSTOMER_ID
                 , item.NAME
                 , item.EMAIL
                 , item.COUNTRY_NAME
                 , item.BUDGET
                 , item.USED));
                }
            }

            callMenu();
        }

        public static void insertData()
        {
            //2.Insert ข้อมูล Table [dbo].[CUSTOMER]
            using (var db = new EF_Model())
            {
                try
                {
                    db.CUSTOMER.Add(new CUSTOMER()
                    {
                        CUSTOMER_ID = "C006",
                        NAME = "Missle Mine",
                        EMAIL = "Golopop@gmail.com",
                        COUNTRY_CODE = "TH",
                        BUDGET = 4580000,
                        USED = 0
                    });
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.InnerException.InnerException.Message);
                }
                Console.WriteLine("insert success");

            }
            showData();
            callMenu();
        }

        public static void updateData()
        {
            //3.Update ข้อมูล Table [dbo].[CUSTOMER] กำหนดค่า [BUDGET]=0, [USED]=1 โดยมีเงื่อนไข [NAME] มีตัว J ในชื่อ
            using (var db = new EF_Model())
            {

                try
                {
                    var update = db.CUSTOMER.Where(o => o.NAME.Contains("j")).ToList();
                    if (update != null)
                    {
                        foreach (var item in update)
                        {
                            item.BUDGET = 0;
                            item.USED = 1;
                        }
                    }
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.InnerException.InnerException.Message);
                }
            }
            Console.WriteLine("UPDATE SUCCESS");
            showData();
            callMenu();
        }

        public static void deleteData()
        {
            //4.Delete ข้อมูล ที่ Insert เข้าไปล่าสุด 1 Row
            using (var db = new EF_Model())
            {

                try
                {
                    //var subquery = db.Database.SqlQuery<CustomerInfo>("SELECT Count(*) AS ROW FROM CUSTOMER").FirstOrDefault();
                    string strSQL = string.Empty;
                    strSQL = "Delete FROM CUSTOMER";
                    strSQL += " Where CUSTOMER_ID = (SELECT TOP 1 CUSTOMER_ID FROM CUSTOMER ORDER BY CUSTOMER_ID DESC)";
                    // Console.WriteLine(subquery.Row);
                    db.Database.ExecuteSqlCommand(strSQL);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            Console.WriteLine("Delete Success");
            showData();
            callMenu();

        }

        public static void callMenu()
        {
            Console.Write("1.Select Data" +
               " 2.Insert Data" +
               " 3.Update Data" +
               " 4.Delete Data" +
               " 5.EXIT!!  :");
            var choice = Console.ReadLine();
            switch (choice)
            {
                case "1": selectData(); break;
                case "2": insertData(); break;
                case "3": updateData(); break;
                case "4": deleteData(); break;
                case "5": break;
            }
        }
        static void Main(string[] args)
        {
            callMenu();
            //Console.WriteLine("Enter some key to exit");
            //Console.ReadLine();

        }
    }
}
